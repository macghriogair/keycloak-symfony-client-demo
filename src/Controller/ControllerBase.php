<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class ControllerBase extends AbstractController
{
    protected function getAuthProvider(array $options = [])
    {
        $options = array_merge([
            'authServerUrl'         => $this->getParameter('kc_auth_server_url'),
            'realm'                 => $this->getParameter('kc_realm'),
            'clientId'              => $this->getParameter('kc_client_id'),
            'clientSecret'          => $this->getParameter('kc_client_secret'),
            'redirectUri'           => 'http://localhost:8099/auth/login',
//          'encryptionAlgorithm'   => 'ES256',                             // optional
//          'encryptionKeyPath'     => '../key.pem'                         // optional
//          'encryptionKey'         => 'contents_of_key_or_certificate'     // optional
        ], $options);

        return new \Stevenmaguire\OAuth2\Client\Provider\Keycloak($options);
    }
}