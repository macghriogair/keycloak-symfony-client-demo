<?php

namespace App\Controller;

use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends ControllerBase
{
    /**
     * @Route("/", name="default")
     */
    public function index(SessionInterface $session): Response
    {
        // need to login?
        if (! $session->has('token')) {
            return $this->redirectToRoute('login');
        }

        /** @var AccessToken $token */
        $token = $session->get('token');

        $provider = $this->getAuthProvider();
        if ($token instanceof AccessToken === false) {
           throw new \RuntimeException('Unexpected token type. Got ' . get_class($token));
        }

        // need to refresh token?
        if ($token->hasExpired()) {
            return $this->redirectToRoute('refresh');
        }

        try {
            $user = $provider->getResourceOwner($token);
            if ($user instanceof ResourceOwnerInterface === false) {
                throw new \RuntimeException('Unexpected user type. Got ' . get_class($user));
            }
        } catch (\Exception $e) {
            $session->remove('token');
            dd($e);
        }

        $parts = explode('.', $token->getToken());
        $decodedHeader = json_decode(base64_decode($parts[0]));
        $decodedPayload = json_decode(base64_decode($parts[1]));

        return $this->json([
            'message' => 'Welcome ' . $user->getName(),
            'userinfo' => $user->toArray(),
            'token' => $token,
            'tokenHeader' => $decodedHeader,
            'tokenPayload' => $decodedPayload,
            'refreshTokenUrl' => 'http://localhost:8099/auth/refresh/',
            'logoutUrl' => 'http://localhost:8099/auth/logout/'
        ]);
    }
}
