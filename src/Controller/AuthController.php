<?php

namespace App\Controller;

use Exception;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends ControllerBase
{
    /**
     * @Route("/auth/login", name="login")
     */
    public function loginAction(Request $request, SessionInterface $session): Response
    {
        $provider = $this->getAuthProvider();

        if (!$request->get('code')) {
            // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl();
            $session->set('oauth2state', $provider->getState());

            return $this->redirect($authUrl);

            // Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($request->get('state')
            || ($session->has('oauth2state') && $request->get('state') !== $session->get('oauth2state')))) {

            $session->remove('oauth2state');
            exit('Invalid state, make sure HTTP sessions are enabled.');
        }
        // Try to get an access token (using the authorization coe grant)
        try {
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $request->get('code')
            ]);
        } catch (Exception $e) {
            exit('Failed to get access token: '.$e->getMessage());
        }

        // TODO: use token to authenticate with Symfony Firewall instead, this is just for demo
        $session->set('token', $token);

        // Optional: Now you have a token you can look up a users profile data
        try {

            // We got an access token, let's now get the user's details
            // $user = $provider->getResourceOwner($token);

            return $this->redirectToRoute('default');
        } catch (Exception $e) {
            exit('Failed to get resource owner: '.$e->getMessage());
        }
    }

    /**
     * @Route("/auth/refresh", name="refresh")
     */
    public function refreshAction(Request $request, SessionInterface $session): Response
    {
        try {
            if (!$session->has('token')) {
                throw new \RuntimeException('Token missing.');
            }

            $token = $session->get('token');
            if ($token instanceof AccessToken === false) {
                throw new \RuntimeException('Unexpected token type: ' . get_class($token));
            }

            $token = $this
                ->getAuthProvider()
                ->getAccessToken(
                    'refresh_token',
                    ['refresh_token' => $token->getRefreshToken()]
                );
            $session->set('token', $token);
        } catch (\Exception $e) {
            dump($e);
            $session->remove('token');
            $session->invalidate();
        }

        return $this->redirectToRoute('default');
    }

    /**
     * @Route("/auth/logout", name="logout")
     */
    public function logoutAction(Request $request, SessionInterface $session): Response
    {
        if (empty($request->get('state'))
            && $session->has('token')
            && $session->get('token') instanceof AccessToken) {
            $provider = $this->getAuthProvider([
                'redirectUri' => 'http://localhost:8099/auth/logout',
            ]);
            $logoutUrl =$provider->getLogoutUrl();

            return $this->redirect($logoutUrl);
        } elseif ($request->get('state')) {
            $session->remove('token');
            $session->invalidate();

            return $this->json([
                'message' => 'logged out with success',
                'redirectTo' => 'http://localhost:8099/',
            ]);
        } else {
            throw new \RuntimeException('Something went wrong during logout.');
        }
    }
}
