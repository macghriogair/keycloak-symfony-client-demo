# Keycloak Symfony Demo

## How to

1. `composer install`

2. add env vars to `.env.local`

```
KC_AUTH_SERVER_URL=https://keycloak.basilicom.io/auth
KC_REALM=<KEYCLOAK-REALM>
KC_CLIENT_ID=<ID-OF-KEYCLOAK-CLIENT>
KC_CLIENT_SECRET=<KEYCLOAK-CLIENT-TOKEN>
```

3. Start PHP server: `symfony server:start --port=8099`

4. Go to: http://127.0.0.1:8099